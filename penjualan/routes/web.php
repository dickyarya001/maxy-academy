<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'penjualan'], function () use ($router) {
    $router->get('/', function () {
        return response()->json([
            [
                "id" => "1",
                "nomor" => "SALE/0001",
                "customer" => "Dicky"
            ],

            [
                "id" => "2",
                "nomor" => "SALE/0002",
                "customer" => "jhon"
            ],

            [
                "id" => "3",
                "nomor" => "SALE/0003",
                "customer" => "joe"
            ],

            [
                "id" => "4",
                "nomor" => "SALE/0004",
                "customer" => "jay"
            ],
        ]);
        // Matches The "/admin/users" URL
    });
    $router->get('/{id}', function ($id) {
        return response()->json(['data' => [
            "id" => "1",
            "nomor" => "SALE/0001",
            "customer" => "Dicky",
            "total" => "2000000",
            "alamat" => "purwokerto"
        ],]);
    });

    $router->post('/', function () {
        return response()->json([
            'msg' => "Berhasil",
            'id' => 1
        ]);
    });

    $router->put('/{id}', function (Request $request, $id) {
        $nomor = $request->input('nomor');
        return response()->json(['data' => [
            "id" => $id,
            "nomor" => $nomor,
            "customer" => "Dicky",
            "total" => "2000000",
            "alamat" => "purwokerto"
        ],]);
    });

    $router->delete('/{id}', function ($id) {
        return response()->json(['data' => "Behasil Delete"]);
    });

    $router->get('/{id}/confirm', function (Request $request, $id) {
        $user = $request->user();
        Log::debug($user);
        Log::debug(">>>>>>");
        if ($user == null) {
            return response()->json(['error' => 'Unauthorized'], 401, ['X-Header-One' => 'Header Value']);
        }
        return response()->json(['msg' => "berhasil confirm"]);
    });

    $router->get('/{id}/send-email', function (Request $request, $id) {
        $user = $request->user();
        Mail::row('This is the email body.', function ($message) {
            $message->to('testdiki@yopmail.com')
                ->subject('Lumen email test');
        });

        return response()->json(['msg' => "berhasil kirim email"]);
    });
});
